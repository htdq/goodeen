<#import "macro.ftl" as m>
<@c.html title="Goodeen" route="route-profile"
	extCssfiles=["core/comment.css",
		"jquery/emojione/emojione.sprites.css",
		"jquery/emojione/area/emojionearea.css"]
	extJsfiles=["jquery/emojione/emojione.js",
		"jquery/emojione/area/emojionearea.js",
		"core/follow.js",
		"jquery/infinitescroll.js",
		"core/infinitescroll.js"]>
	<div class="col-xs-8">
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">
		    	<strong>行程动态</strong>
				</h3>
		  </div>
		  <div class="panel-body stream">
				<@m.tripTempWithWrapper/>
				<@c.streamFooter url="/?page=2" emptyText="没有发现行程。" icon="icon-road" />		
			</div>
		</div>			
	</div>
	<div class="col-xs-4">
		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">
		    	<strong>新加入的谷钉</strong>
				</h3>
		  </div>
		  <div class="panel-body stream">
				<@m.accountTempWithWrapper/>
			</div>
		</div>			
	</div>
</@c.html>
<script type="text/javascript">
    $(function () {
        $(".trip-summary").html(function (n, oldcontent) {
            return emojione.unicodeToImage(oldcontent);
        });

        $("#content").delegate(".trip", "click", function () {
            location.href = "/trip/" + $(this).attr("data-id")
        });
        $("#content").delegate(".icon-reply", "click", function (e) {
            e.stopPropagation();
        });
        $("#content").delegate(".icon-user-add", "click", function (e) {
            e.stopPropagation();
        });
        $("#content").delegate(".icon-star", "click", function (e) {
            e.stopPropagation();
        });
    });
</script>